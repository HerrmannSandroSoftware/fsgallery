	
function openDeleteDialog(title, callback) {
	$( "#deletedialogcontainer" ).dialog({ 
		modal: true,
		width: 300,
		height: 200,
		draggable: false,
		title: title,
		buttons: [{ text: "OK", click: function() {callback();$( "#deletedialogcontainer" ).dialog( "destroy" );}, id: "button-delete-ok" }]
	});
}
