$(function() {
	$("#header #teaseralbums .album:first").addClass("selected");
	showActiveAlbumAsTeaser();
	
	$("#header #teaseralbums .album").hover(function() {
		$("#header #teaseralbums").addClass("hover");
		if(!$(this).hasClass("selected")) {
			$("#header #teaseralbums .album").removeClass("selected");
			$(this).addClass("selected");
			showActiveAlbumAsTeaser();
		}
	},function() {
		$("#header #teaseralbums").removeClass("hover");
	});
		
	setInterval(function(){
		if(!$("#header #teaseralbums").hasClass("hover") && $("#header:in-viewport").length == 1) {
			var nextAlbum = $("#header #teaseralbums .album.selected").next();
			if(nextAlbum.length == 0) {
				nextAlbum = $("#header #teaseralbums .album:first");
			}
			$("#header #teaseralbums .album").removeClass("selected");
			nextAlbum.addClass("selected");
			showActiveAlbumAsTeaser();
		}
	},10000);
	
	function showActiveAlbumAsTeaser() {
		var coverImagePath = $("#header #teaseralbums .album.selected .coverimagepath").val();
		var bigAlbum = $("#header #teaseralbums .album.selected").clone();
		bigAlbum.find("img").attr("src", coverImagePath);
		$("#header #currentteaser").html(bigAlbum);
		$("#header #teasercontainer #backgroundteaser img").attr("src", coverImagePath);
	}
});