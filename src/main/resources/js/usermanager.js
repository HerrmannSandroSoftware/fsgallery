$(function() {
	
	$("#link-usermanager").click(openUsermanagerDialog);
	
	function openUsermanagerDialog() {
		$( "#usermanagerdialogcontainer" ).dialog({ 
			modal: true,
			height: 600,
			width: 800,
			open: dialogOpened,
			draggable: false,
			title: $("#usermanager-dialog-title").val()
		});
	}

	function dialogOpened() {
	    
		$("#userpermissiontable").on("change", "input.permission", savePermissions);
		$("#usermanagertabs").tabs();
		$("#usermanagertabs #userpermissiontable tbody tr:not(.hidden)").remove();
		$.post("usermanager/getuserdata", function(data) {
			for (var userIndex in data.users) {
				var user = data.users[userIndex];
				var clonedLine = $("#userpermissiontable tbody tr:first").clone().removeClass("hidden");
				$("#userpermissiontable tbody").append(clonedLine);
				clonedLine.find("input.username").val(user.username);
				clonedLine.find("span.username-show").text(user.username);
				setUserPermissions(clonedLine, data.permissions, user);
			}
			
		});
		
		$("#userinvitationform").submit(function(e) {
			e.preventDefault();
			$.post("usermanager/generateinvitation?"+$(this).serialize(), function(data) {
				$("#userinvitationlinkarea").text(data.invitationlink);
				loadInvitations();
			});
			return false;
		});
		
		loadInvitations();
	}
	
	function loadInvitations() {
		$("#userinvitationlist").load("usermanager/getinvitations", function() {
			
			$("#userinvitationlist button.button-invitationdelete").click(deleteInvitation);
			$("#userinvitationlist a.invitationlink").click(function(e){
				e.preventDefault();
				$("#userinvitationlinkarea").text($(this).attr("href"));
				return false;
			});
		});
	}
		
	function deleteInvitation() {
		$.post("usermanager/deleteinvitation/"+$(this).attr("invitationid"), function(data) {
			loadInvitations();
		});
	}
	
	function savePermissions() {
		
		var username = $(this).closest("tr").find(".username").val();
		var permission = $(this).attr("name");
		var request = "";
		if($(this).is(':checked')) {
			request = "usermanager/setpermission/"+username+"/"+permission
		} else {
			request = "usermanager/removepermission/"+username+"/"+permission
		}
		$.post(request, function(data) {
			console.log(data);
		});
	}
	
	function setUserPermissions(line, permissions, user) {
	    for (var key in permissions) {
            var userList = permissions[key];
        
            for (var userIndex in userList) {
                var userFromStructure = userList[userIndex];
                if(userFromStructure.username == user.username) {
                    line.find("input.permission[name='"+key+"']").attr("checked", "checked");
                }
            } 
        
	    }
	}

});