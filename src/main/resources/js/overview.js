var timeline;

$(function() {
	$.post("album/get/timeline", function(data) {
		timeline = data.timeline;
		$(window).bind("scroll", fillAlbums);
        $(window).bind("scroll", setYearSelector);
        fillAlbums();
        setYearSelector();

        function fillAlbums() {
        	$("#albumtimeline .month:not(.loaded):in-viewport").each(function() {
        		var month = $(this).attr("month");
        		var year = $(this).closest(".year").attr("year");
        		var albumArray = data.timeline[year+"-"+month];
        		for (var albumIndex in albumArray) {
        			var album = albumArray[albumIndex];
        			var albumHtml = $("#albumtemplate").clone().attr("id", album.storageid).removeClass("hidden");
        			albumHtml.find(".coverimage").attr("src", "pictures/"+album.storageid+"/thumbs/"+album.coverimage);
        			albumHtml.find(".albumname").text(album.name);
        			$(this).children(".clear").before(albumHtml);
        			albumHtml.find(".deletealbumlink").click(function(e){deleteAlbum(e, $(this))});
        		}
        		$(this).addClass("loaded");
        	});
        }
        
        function deleteAlbum(e, link) {
        	e.preventDefault();
        	e.stopPropagation();
        	var albumId = link.closest(".album").attr("id");
        	var title = link.attr("title");
        	openDeleteDialog(title, function(){
        		$.post("album/delete/"+albumId, function(data) {
        			if(data.status == 0) {
        				$("#teaseralbums #"+albumId).remove();
    					$("#albumtimeline #"+albumId).remove();
    				}
        		});
        	});
        	return false;
        }
        
        function setYearSelector() {
        	var viewportYear = $("#albumtimeline .year:in-viewport:first").attr("year");
        	$("#albumquickjump .yearquickentry").removeClass("active");
        	$("#albumquickjump .yearquickentry."+viewportYear).addClass("active");
        	
        	if($("#header:above-the-top").length > 0) {
        		$("#albumquickjump .yearline").addClass("fixed");
        	} else {
        		$("#albumquickjump .yearline").removeClass("fixed");
        	}
        }
    });
}); 