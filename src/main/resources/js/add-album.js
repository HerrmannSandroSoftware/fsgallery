$(function() {
	
	$("#link-add-album").click(openAlbumDialog);
	
	$("#rawalbumsearchfield").keyup(searchRawAlbums);

	function searchRawAlbums() {
		console.log($(this).val());
		$("#rawalbumslist .rawalbum").show();
		if($(this).val().length > 0) {
			
			$("#rawalbumslist .rawalbum:not([lowercasedname*="+$(this).val()+"])").hide();
		} 		
	}
	
	function openAlbumDialog() {
		$( "#addalbumdialogcontainer" ).dialog({ 
			modal: true,
			height: 400,
			width: 500,
			open: dialogOpened,
			draggable: false,
			title: $("#addalbum-dialog-title").val(),
			buttons: [ { text: $("#addalbum-dialog-back").val(), click: showRawAlbums, id: "button-addalbum-back", },
			           { text: $("#addalbum-dialog-next").val(), click: insertAlbumDetails, id: "button-addalbum-next", },
			           { text: $("#addalbum-dialog-done").val(), click: showImportStatus, id: "button-addalbum-finish", }]
		});
	}
	
	function dialogOpened( event, ui ) {
		
		//cleanup the dialog
		$("#rawalbumslist").html("");
		$("#addalbumdetailsform").get(0).reset();
				
		$.post("album/add/getrawalbums", function(data) {
            for (var key in data) {
            	var value = data[key];
	           	$("#rawalbumslist").append("<a lowercasedname='"+key.toLowerCase()+"' href='#' modified='"+value.modified+"' class='rawalbum'><img src='resources/images/rawalbum.png' /><span class='rawalbumname'>"+key+"</span></a>")
            }
            
            $("#rawalbumslist a").sortElements(function(a, b){
                return $(a).attr("modified") < $(b).attr("modified") ? 1 : -1;
            });
            
            $("#rawalbumslist .rawalbum").click(function(event) {
            	event.preventDefault();
            	$("#rawalbumslist .rawalbum").removeClass("selected");
            	$(this).addClass("selected");
            	var rawAlbumname = $(this).children(".rawalbumname").text();
            	$("#rawalbumnamehidden").val(rawAlbumname);
            	$("#newalbumname").val(rawAlbumname);
            	$("#rawalbumnameformfield").text(rawAlbumname);
            	$("#button-addalbum-next").button("enable");
            	return false;
            });
		});
		
		$("#newalbumdate").datepicker({ dateFormat: "yy-mm-dd" });
		
		showRawAlbums();
	}
	
	function showRawAlbums() {
		$("#addalbumdialogcontainer .stepview").removeClass("active");
		$("#addalbumstep1").addClass("active");
		$("#button-addalbum-back").button("disable");
		
		if($("#rawalbumslist .rawalbum.selected").length == 0) {
			$("#button-addalbum-next").button("disable");
		} else {
			$("#button-addalbum-next").button("enable");
		}
		
		$("#button-addalbum-finish").button("disable");
		
		
	}
	
	function insertAlbumDetails() {
		$("#addalbumdialogcontainer .stepview").removeClass("active");
		$("#addalbumstep2").addClass("active");
		$("#button-addalbum-back").button("enable");
		$("#button-addalbum-next").button("disable");
		$("#button-addalbum-finish").button("enable");
		
	}
	
	function showImportStatus() {
		$("#addalbumdialogcontainer .stepview").removeClass("active");
		$("#addalbumstep3").addClass("active");
		$("#button-addalbum-back").button("disable");
		$("#button-addalbum-next").button("disable");
		$("#button-addalbum-finish").button("disable");
		
		var progressBar = $( "#addalbumprogressbar" );
		var progressLabel = $( "#addalbumprogressbar .progress-label" );
		progressBar.progressbar({
				value: false,
			    change: function() {
			    	progressLabel.text( progressBar.progressbar( "value" ) + "%" );
		        },
			    complete: function() {
			    	progressLabel.text( "Complete!" );
			    }
	    });
		
		$.post("album/add/addalbum?"+$("#addalbumdetailsform").serialize(), function(data) {
			addAlbumStatusTicker(progressBar)
		});
	}
	
	function addAlbumStatusTicker(progressBar) {
		
		$.post("album/add/status", function(data) {
			console.log("Import Status");
			console.log(data);
			console.log("-------------------");
			progressBar.progressbar( "value", data.task.progress );
			
			if(data.task.progress < 100) {
				setTimeout(function(){addAlbumStatusTicker(progressBar);} ,1000);
			} else {
				window.location.reload();
			}
		});
	}
	
}); 

/**
 * jQuery.fn.sortElements
 * --------------
 * @param Function comparator:
 *   Exactly the same behaviour as [1,2,3].sort(comparator)
 *   
 * @param Function getSortable
 *   A function that should return the element that is
 *   to be sorted. The comparator will run on the
 *   current collection, but you may want the actual
 *   resulting sort to occur on a parent or another
 *   associated element.
 *   
 *   E.g. $('td').sortElements(comparator, function(){
 *      return this.parentNode; 
 *   })
 *   
 *   The <td>'s parent (<tr>) will be sorted instead
 *   of the <td> itself.
 */
jQuery.fn.sortElements = (function(){
 
    var sort = [].sort;
 
    return function(comparator, getSortable) {
 
        getSortable = getSortable || function(){return this;};
 
        var placements = this.map(function(){
 
            var sortElement = getSortable.call(this),
                parentNode = sortElement.parentNode,
 
                // Since the element itself will change position, we have
                // to have some way of storing its original position in
                // the DOM. The easiest way is to have a 'flag' node:
                nextSibling = parentNode.insertBefore(
                    document.createTextNode(''),
                    sortElement.nextSibling
                );
 
            return function() {
 
                if (parentNode === this) {
                    throw new Error(
                        "You can't sort elements if any one is a descendant of another."
                    );
                }
 
                // Insert before flag:
                parentNode.insertBefore(this, nextSibling);
                // Remove flag:
                parentNode.removeChild(nextSibling);
 
            };
 
        });
 
        return sort.call(this, comparator).each(function(i){
            placements[i].call(getSortable.call(this));
        });
 
    };
 
})();

