$(function() {
	
	$("body").on("click", ".album", openAlbumDialog);
	
	function openAlbumDialog(e) {
		e.preventDefault();
		
		var albumElement = $(this);
		$( "#showalbumdialogcontainer" ).dialog({ 
			modal: true,
			width: 800,
			height: 720,
			open: function(event, ui) {dialogOpened(event, ui, albumElement.attr("id"))},
			draggable: false,
			title: albumElement.find(".albumname").text()
		});
		return false;
	}
	
	function dialogOpened( event, ui, albumId ) {
		$("#imageandactionscontainer, #showalbumthumbnails").html("");
		$.post("album/get/"+albumId, function(data) {
        	initImageView(data.album);
        	initThumbnailPager(data.album);
        	$("#imageandactionscontainer").unbind().on("click", "a", function(e){
        		e.preventDefault();
        		$("#thumbnailpagerright").trigger("click");
        		return false;
        	});
     	});
	}
		
	function initThumbnailPager(album) {
		$("#thumbnailpagerleft").unbind().click(function(e) {
			e.preventDefault();
			if(album.images.length > 1) {
				$("#showalbumthumbnails").prepend($("#showalbumthumbnails a:last"));
				var selectedElement = $("#showalbumthumbnails a.selected").removeClass("selected");
				if(selectedElement.prev().length > 0) {
                    selectedElement.prev().addClass("selected");
                } else {
                    selectedElement.parent().children(":last").addClass("selected");
                }
				selectedThumnailChanged(album);
			}
			
		});
		
		$("#thumbnailpagerright").unbind().click(function(e) {
			e.preventDefault();
			if(album.images.length > 1) {

			    $("#showalbumthumbnails").append($("#showalbumthumbnails a:first"));
			    var selectedElement = $("#showalbumthumbnails a.selected").removeClass("selected");
			    if(selectedElement.next().length > 0) {
			        selectedElement.next().addClass("selected");
			    } else {
			        selectedElement.parent().children(":first").addClass("selected");
			    }
				
			    selectedThumnailChanged(album);
			}
			return false;
		});
		
		$("#showalbumthumbnails a").click(function(e){
			e.preventDefault();
			$("#showalbumthumbnails a").removeClass("selected");
			$(this).addClass("selected");
			selectedThumnailChanged(album);
			return false;
		});
		$("#showalbumthumbnails a:first").addClass("selected");
		selectedThumnailChanged(album);
	}
	
	function selectedThumnailChanged(album) {
		var selectedThumbnail = $("#showalbumthumbnails .selected");
		var index = selectedThumbnail.attr("index");
		if(index) {
			$("#imageandactionscontainer").html("<a class='albumpicture' href='#' index='"+index+"'><img class='albumpictureimg' src='pictures/"+album.storageid+"/"+album.images[index]+"'></a>");
			$("#imageandactionscontainer img").load(imageLoaded);
			initImageActions(album, album.images[index]);
		} else {
			$("#imageandactionscontainer").html(selectedThumbnail.html());
		}
	}
	
	function imageLoaded() { 
		$("#imageandactionscontainer").css("width", $("#imageandactionscontainer .albumpicture").width()+"px");
	}

	function initImageActions(album, image) {
		$("#imageandactionscontainer").append(jQuery("#imageactionscontainer").html());
		initCommentArea(album, image);
		initCoverImageArea(album, image);
	}
	
	function changeCoverImageInTimeline(albumid, coverimage) {
		
		for (var key in timeline) {
			
			albumArray = timeline[key];
			
			for (var albumIndex in albumArray) {
				var album = albumArray[albumIndex];
				if(album.storageid == albumid) {
					album.coverimage = coverimage;
				}
			}
		}
	}
	
	function initCommentArea(album, image) {
		var commenttext = $("#imageandactionscontainer .commenttext");
		
		var commentinput = $("#imageandactionscontainer .commentinput");
		
		commenttext.text(album.comments[image]);
		
		$("#imageandactionscontainer .writecommentlink").click(function(e){
			e.preventDefault();
			$(this).hide();
			commenttext.hide();
			$("#imageandactionscontainer .savecommentlink").show();
			$("#imageandactionscontainer .commentinput").show();
			commentinput.val(album.comments[image]);
			return false;
		});
		
		$("#imageandactionscontainer .savecommentlink").click(function(e){
			e.preventDefault();
			$(this).hide();
			commenttext.show();
			commentinput.hide();
			$("#imageandactionscontainer .writecommentlink").show();
			
			$.post("album/imageactions/comment/"+album.storageid+"?image="+image+"&comment="+commentinput.val(), function(data) {
				if(data.status == 0) {
					commenttext.text(commentinput.val());
					album.comments[image] = commentinput.val();
				} else {
					commenttext.text("error!");
				}
			});
			
			
			return false;
		});
		
	}
	
	function initCoverImageArea(album, image) {
		var button = $("#imageandactionscontainer button");
		
		if(image.toLowerCase() == album.coverimage.toLowerCase()) {
			button.attr("disabled", "disabled");
		}
		
		button.click(function() {
			$.post("album/imageactions/setcoverimage/"+album.storageid+"/"+image, function(data) {
				if(data.status == 0) {
					button.attr("disabled", "disabled");
					album.coverimage = image;
					$("#teaseralbums #"+album.storageid+" img").attr("src","pictures/"+album.storageid+"/thumbs/"+image);
					$("#teaseralbums #"+album.storageid+" .coverimagepath").val("pictures/"+album.storageid+"/"+image);
					$("#albumtimeline #"+album.storageid+" img").attr("src","pictures/"+album.storageid+"/thumbs/"+image);
					changeCoverImageInTimeline(album.storageid, image);
				}
			});
		});
	}
	
	function initImageView(album) {
		$("#imageandactionscontainer").html("<a class='albumpicture' href='#' index='"+0+"'><img class='albumpictureimg' src='pictures/"+album.storageid+"/"+album.images[0]+"'></a>");
		initImageActions(album, album.images[0]);
		for (var imageIndex in album.images) {
        	var imageName = album.images[imageIndex];
        	$("#showalbumthumbnails").append("<a href='#' class='thumbnail' index='"+imageIndex+"'><img src='pictures/"+album.storageid+"/thumbs/"+imageName+"'></a>");
        }
		$("#showalbumthumbnails").append("<a href='#' class='thumbnail end' >"+$("#endofalbum").val()+"</a>");
        
	}
	
}); 

