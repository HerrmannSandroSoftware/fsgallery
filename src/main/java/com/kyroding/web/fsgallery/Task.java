package com.kyroding.web.fsgallery;

import java.util.Set;
import java.util.UUID;
import java.util.concurrent.CopyOnWriteArraySet;

public abstract class Task extends Thread {
	private final Set<TaskCompleteListener> listeners = new CopyOnWriteArraySet<TaskCompleteListener>();
	protected int progress=0;
	protected String status = "initialized";

	public String getTaskId() {
		return UUID.randomUUID().toString();
	}
	
	public final void addListener(final TaskCompleteListener listener) {
		listeners.add(listener);
	}
  
	public final void removeListener(final TaskCompleteListener listener) {
		listeners.remove(listener);
	}
  
	private final void notifyListeners() {
		for (TaskCompleteListener listener : listeners) {
			listener.notifyOfTaskComplete(this);
		}
	}
  
	@Override
	public final void run() {
		try {
			status = "running";
			doRun();
			status = "finished";
		} finally {
			notifyListeners();
		}
	}
  
	public abstract void doRun();
	
	public String getStatus() {
		return status;
	}
	
	public int getProgress() {
		return progress;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof Task) {
			return ((Task)obj).getTaskId().equals(this.getTaskId());
		}
		return super.equals(obj);
	}
	
	@Override
	public String toString() {
		return this.getClass().getSimpleName()+" "+getTaskId()+" "+getStatus()+" "+getProgress();
	}
}