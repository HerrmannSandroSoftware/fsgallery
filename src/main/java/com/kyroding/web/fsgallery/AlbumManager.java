package com.kyroding.web.fsgallery;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.TreeMap;

import org.apache.commons.io.FileUtils;


public class AlbumManager {
	
	private static final String FILE_ALBUM_PROPERTIES = "album.properties";
	private static AlbumManager instance;
	private Map<Integer, List<Integer>> structureMap;
	private Map<String, List<Map<String,Object>>> dateBasicAlbumsMap;
	private Map<String, Map<String,Object>> flatAlbumsMap;
	private List<Map<String,Object>> flatAlbumsList;
	public static final String KEY_ALBUM_STORAGEID = "album.storageid";
	public static final String KEY_ALBUM_TIMESTAMP = "album.timestamp";
	public static final String KEY_ALBUM_NAME = "album.name";
	public static final String KEY_ALBUM_COVERIMAGE = "album.coverimage";
	
	public static final String KEY_VIEW_ALBUM_IMAGES = "images";
	public static final String KEY_VIEW_ALBUM_STORAGEID = "storageid";
	public static final String KEY_VIEW_ALBUM_TIMESTAMP = "timestamp";
	public static final String KEY_VIEW_ALBUM_NAME = "name";
	public static final String KEY_VIEW_ALBUM_COVERIMAGE = "coverimage";
	private static final String KEY_VIEW_ALBUM_COMMENTS = "comments";
	
	
	private AlbumManager() {
		buildCache();
	}
	
	private void buildCache() {
		structureMap = new TreeMap<Integer, List<Integer>>(Collections.reverseOrder());
		dateBasicAlbumsMap = new TreeMap<String, List<Map<String,Object>>>(Collections.reverseOrder());
		flatAlbumsMap = new HashMap<String, Map<String,Object>>();
		flatAlbumsList = new ArrayList<Map<String,Object>>();
		
		File galleryWorkingDirectory = RequestFacade.getGalleryWorkingDirectory();
		List<File> descriptorFilelist = new ArrayList<File>();
		recursiveAlbumDesriptorFinder(galleryWorkingDirectory, descriptorFilelist);
		
		for( File albumdescriptorFile : descriptorFilelist) {
			
			Properties albumdescriptor = getPropertiesFromFile(albumdescriptorFile);
			Calendar cal = Calendar.getInstance();
			cal.setTimeInMillis(new Long(albumdescriptor.getProperty(KEY_ALBUM_TIMESTAMP)));
			
			int year = cal.get(Calendar.YEAR);
			if(structureMap.get(year) == null) {
				structureMap.put(year, new ArrayList<Integer>());
			}
			List<Integer> monthArray = structureMap.get(year);
			
			int month = cal.get(Calendar.MONTH)+1;
			if(!monthArray.contains(month)) {
				monthArray.add(month);
				Collections.sort(monthArray, Collections.reverseOrder());
			}

			File albumFolder = albumdescriptorFile.getParentFile();
		
			//basic album Map
			Map<String, Object> basicAlbum = new HashMap<String, Object>();
			basicAlbum.put(KEY_VIEW_ALBUM_STORAGEID, albumFolder.getName());
			basicAlbum.put(KEY_VIEW_ALBUM_NAME, albumdescriptor.getProperty(KEY_ALBUM_NAME));
			basicAlbum.put(KEY_VIEW_ALBUM_TIMESTAMP, albumdescriptor.getProperty(KEY_ALBUM_TIMESTAMP));
			basicAlbum.put(KEY_VIEW_ALBUM_COVERIMAGE, albumdescriptor.getProperty(KEY_ALBUM_COVERIMAGE));
			
			List<Map<String, Object>> albumsList = dateBasicAlbumsMap.get(year+"-"+month);
			if(albumsList == null) {
				albumsList = new ArrayList<Map<String,Object>>();
				dateBasicAlbumsMap.put(year+"-"+month, albumsList);
			}
			flatAlbumsList.add(basicAlbum);
			albumsList.add(basicAlbum);
			Collections.sort(albumsList, new SimpleAlbumComparator());
		
			//detailed Album List
			Map<String, Object> detailedAlbum = new HashMap<String, Object>();
			detailedAlbum.put(KEY_VIEW_ALBUM_STORAGEID, albumFolder.getName());
			detailedAlbum.put(KEY_VIEW_ALBUM_NAME, albumdescriptor.getProperty(KEY_ALBUM_NAME));
			detailedAlbum.put(KEY_VIEW_ALBUM_TIMESTAMP, albumdescriptor.getProperty(KEY_ALBUM_TIMESTAMP));
			detailedAlbum.put(KEY_VIEW_ALBUM_COVERIMAGE, albumdescriptor.getProperty(KEY_ALBUM_COVERIMAGE));
			detailedAlbum.put(KEY_VIEW_ALBUM_COMMENTS, getAlbumComments(albumFolder.getName()));
			
			List<String> imageNames = Arrays.asList(albumFolder.list(new PictureFileNameFilter()));
			detailedAlbum.put(KEY_VIEW_ALBUM_IMAGES, imageNames);
			
			
			flatAlbumsMap.put(albumFolder.getName(), detailedAlbum);
		}
		Collections.sort(flatAlbumsList, new SimpleAlbumComparator());
		
		System.out.println("Album cache built");
	}
	
	
	private Map<String, String> getAlbumComments(String albumId) {
		Map<String, String> result = new HashMap<String, String>();
		try {
			File albumCommentsFile = getAlbumCommentsFile(albumId);
			if (albumCommentsFile.exists() || (!albumCommentsFile.exists() && albumCommentsFile.createNewFile())) {
				Properties propertiesFromFile = GalleryUtils.getPropertiesFromFile(albumCommentsFile);
				for( Entry<Object, Object> entry : propertiesFromFile.entrySet() ) {
					result.put(entry.getKey().toString(), entry.getValue().toString());
				}
			}
				
		} catch (IOException e) {
			System.err.println("Error reading AlbumComments for album "+ albumId + " because of:"+e.getMessage());
		}
		return result;
	}
	
	public Map<Integer, List<Integer>> getStructuredTimeline() {
		return structureMap;
	}
	
	public Map<String, List<Map<String,Object>>> getAlbumsMap() {
		return dateBasicAlbumsMap;
	}
	
	public void rebuildCache() {
		buildCache();
	}
	
	private Properties getPropertiesFromFile(File file) {
		Properties currentPropertyFile = new Properties();
		try {
			FileInputStream fileStream = new FileInputStream(file);
			InputStreamReader inputStreamReader = new InputStreamReader(fileStream, ViewRenderer.DEFAULT_ENCODING);
			currentPropertyFile.load( inputStreamReader);
			inputStreamReader.close();
			fileStream.close();
		}
		catch (Exception e) {
			System.err.println("Unable to load property file: "+file.getAbsolutePath());
		} 
		return currentPropertyFile;
	}

	private void recursiveAlbumDesriptorFinder(File folder, List<File> fileList) {
		for( File currentFile : folder.listFiles()) {
			if(currentFile.isFile() && currentFile.getName().equals(FILE_ALBUM_PROPERTIES)) {
				fileList.add(currentFile);
			} else if(currentFile.isDirectory()) {
				recursiveAlbumDesriptorFinder(currentFile, fileList);
			}
		}
	}
	
	public List<Map<String, Object>> getRecentAlbums(int count) {
		
		List<Map<String,Object>> resultList = new ArrayList<Map<String,Object>>();
		
		for ( Map<String,Object> album : flatAlbumsList) {
			if(resultList.size() < count) {
				resultList.add(album);
			} else {
				break;
			}
		}
		return resultList;
	}
	
	public static final AlbumManager getInstance() {
		
		if(instance == null) {
			instance = new AlbumManager();
		}		
		return instance;
	}
	
	private class SimpleAlbumComparator implements Comparator<Map<String, Object>> {

		public int compare(Map<String, Object> o1, Map<String, Object> o2) {
			
			if(o1 != null && o2 != null) {
				Object timestamp2 = o2.get(KEY_VIEW_ALBUM_TIMESTAMP);
				Object timestamp1 = o1.get(KEY_VIEW_ALBUM_TIMESTAMP);
				return timestamp2.toString().compareTo(timestamp1.toString());
			}
			return 0;
		}
	}

	public Map<String, Object> getAlbumById(String albumId) {
		return flatAlbumsMap.get(albumId);
	}

	private File getAlbumCommentsFile(String albumId) {
		return new File(RequestFacade.getGalleryWorkingDirectory().getAbsolutePath() + File.separator
				+ albumId + File.separator + "comments.properties");
	}
	
	public boolean addImageComment(String albumId, String imageName, String comment) {
	
		File albumCommentsFile = getAlbumCommentsFile(albumId);
		try {
			if (albumCommentsFile.exists() || (!albumCommentsFile.exists() && albumCommentsFile.createNewFile())) {
				
				Properties propertiesFromFile = GalleryUtils.getPropertiesFromFile(albumCommentsFile);
				propertiesFromFile.setProperty(imageName, comment);
				GalleryUtils.storePropertiesToFile(propertiesFromFile, albumCommentsFile);
				buildCache();
				return true;
			}
		} catch (IOException e) {
			System.err.println("Error writing comment for album "+ albumId+ " and picture "+imageName + " because of:"+e.getMessage());
		}
		return false;
	}

	public boolean setAlbumCoverImage(String albumId, String imageName) {
		
		File albumDescriptorFile = new File(RequestFacade.getGalleryWorkingDirectory().getAbsolutePath() + File.separator
				+ albumId + File.separator + FILE_ALBUM_PROPERTIES);
		try {
			if (albumDescriptorFile.exists() || (!albumDescriptorFile.exists() && albumDescriptorFile.createNewFile())) {
				
				Properties propertiesFromFile = GalleryUtils.getPropertiesFromFile(albumDescriptorFile);
				propertiesFromFile.setProperty(KEY_ALBUM_COVERIMAGE, imageName);
				GalleryUtils.storePropertiesToFile(propertiesFromFile, albumDescriptorFile);
				buildCache();
				return true;
			}
		} catch (IOException e) {
			System.err.println("Error writing comment for album "+ albumId+ " and picture "+imageName + " because of:"+e.getMessage());
		}
		return false;
	}

	public boolean deleteAlbum(String albumId) {
		File albumDescriptorFile = new File(RequestFacade.getGalleryWorkingDirectory().getAbsolutePath() + File.separator
				+ albumId);	
		
		//check if we really have a fsgallery album folder
		if(albumDescriptorFile.exists() && new File(albumDescriptorFile.getAbsoluteFile()+File.separator+FILE_ALBUM_PROPERTIES).exists()) {
			
			try {
				FileUtils.deleteDirectory(albumDescriptorFile);
				buildCache();
				return true;
			} catch (IOException e) {
				System.err.println("Error deleting album with id "+ albumId+ " because of :"+e.getMessage());
			}
		}
		return false;
	}
}
