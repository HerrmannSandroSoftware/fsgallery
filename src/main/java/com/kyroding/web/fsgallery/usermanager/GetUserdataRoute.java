package com.kyroding.web.fsgallery.usermanager;

import java.util.Map;
import java.util.TreeMap;

import spark.Request;
import spark.Response;

import com.kyroding.web.fsgallery.Permission;
import com.kyroding.web.fsgallery.RequestFacade;
import com.kyroding.web.fsgallery.UserManager;
import com.kyroding.web.fsgallery.route.JsonRoute;

public class GetUserdataRoute extends JsonRoute {

    public GetUserdataRoute() {
        super(RequestFacade.getInstance().getContextPath() + "/usermanager/getuserdata");
    }

    @Override
    public Map<String, Object> buildJsonData(Request request, Response response) {
        Map<String, Object> result = new TreeMap<String, Object>();
        result.put("users", UserManager.getInstance().getUsers());
        result.put("permissions", UserManager.getInstance().getPermissionMap());
        return result;
    }

    @Override
    public Permission getPermission() {
        return Permission.MANAGE_USERS;
    }

}
