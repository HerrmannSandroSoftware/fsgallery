package com.kyroding.web.fsgallery.usermanager;

import java.util.Map;
import java.util.TreeMap;

import spark.Request;
import spark.Response;

import com.kyroding.web.fsgallery.Permission;
import com.kyroding.web.fsgallery.RequestFacade;
import com.kyroding.web.fsgallery.User;
import com.kyroding.web.fsgallery.UserManager;
import com.kyroding.web.fsgallery.route.JsonRoute;

public class RemoveUserPermissionRoute extends JsonRoute {

    public RemoveUserPermissionRoute() {
        super(RequestFacade.getInstance().getContextPath() + "/usermanager/removepermission/*/*");
    }

    @Override
    public Map<String, Object> buildJsonData(Request request, Response response) {
        Map<String, Object> result = new TreeMap<String, Object>();
        String[] splat = request.splat();
        if (splat.length == 2) {
            String userName = splat[0];
            String permissionName = splat[1];

            UserManager userManager = UserManager.getInstance();
            User user = userManager.getUser(userName);
            Permission permission = Permission.valueOf(permissionName.toUpperCase());
            userManager.removePermission(user, permission);
            result.put("status", 0);
        }
        return result;
    }

    @Override
    public Permission getPermission() {
        return Permission.MANAGE_USERS;
    }

}
