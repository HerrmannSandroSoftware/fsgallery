package com.kyroding.web.fsgallery.usermanager;

import java.util.Locale;

import spark.Request;
import spark.Response;
import spark.Route;

import com.kyroding.web.fsgallery.RequestFacade;
import com.kyroding.web.fsgallery.User;
import com.kyroding.web.fsgallery.UserManager;

public class SetUserLanguageRoute extends Route {

	public SetUserLanguageRoute() {
		super("/user/setlanguage/*");
	}

	@Override
	public Object handle(Request request, Response response) {
		
		String[] splat = request.splat();
		if(splat.length == 1) {
			 User user = (User)request.session().attribute("user");
		     if(user != null) {
		    	 
		    	 Locale newLocale = new Locale(splat[0]);
				 if(UserManager.getInstance().setUserLanguage(user, newLocale)) {
		    		 user.setLocale(newLocale);
		    		 request.session().attribute("user", user);
		    	 }
		     }
		}
		response.redirect(RequestFacade.getInstance().getContextPath()+"/view");
		return "";
	}

	
}
