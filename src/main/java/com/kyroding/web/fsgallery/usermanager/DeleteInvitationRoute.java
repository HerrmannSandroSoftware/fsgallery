package com.kyroding.web.fsgallery.usermanager;

import java.util.Map;
import java.util.TreeMap;

import spark.Request;
import spark.Response;

import com.kyroding.web.fsgallery.Permission;
import com.kyroding.web.fsgallery.RequestFacade;
import com.kyroding.web.fsgallery.UserManager;
import com.kyroding.web.fsgallery.route.JsonRoute;

public class DeleteInvitationRoute extends JsonRoute {

    public DeleteInvitationRoute() {
        super(RequestFacade.getInstance().getContextPath() + "/usermanager/deleteinvitation/*");
    }

    @Override
    public Map<String, Object> buildJsonData(Request request, Response response) {
        Map<String, Object> result = new TreeMap<String, Object>();
        String[] splat = request.splat();
        if(splat.length == 1) {
        	UserManager.getInstance().deleteInvitation(splat[0]);
        	result.put("status", 0);
        }
        return result;
    }
   
    @Override
    public Permission getPermission() {
        return Permission.MANAGE_USERS;
    }

}
