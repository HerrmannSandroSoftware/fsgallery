package com.kyroding.web.fsgallery.usermanager;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import spark.Request;
import spark.Response;

import com.kyroding.web.fsgallery.Permission;
import com.kyroding.web.fsgallery.RequestFacade;
import com.kyroding.web.fsgallery.UserManager;
import com.kyroding.web.fsgallery.route.JsonRoute;

public class GenerateInvitationRoute extends JsonRoute {

    public GenerateInvitationRoute() {
        super(RequestFacade.getInstance().getContextPath() + "/usermanager/generateinvitation");
    }

    @Override
    public Map<String, Object> buildJsonData(Request request, Response response) {
        Map<String, Object> result = new TreeMap<String, Object>();
        
        Set<Permission> permissions = new HashSet<Permission>();
        for(String param : request.queryParams()) {
        	permissions.add(Permission.valueOf(param.toUpperCase()));
        }
        String initationID = UserManager.getInstance().addInvitation(permissions);
        
        result.put("status", 0);
        result.put("invitationid", initationID);
        String invitatoinLink = getProtocol(request.raw().getProtocol())+"://"+request.raw().getServerName()+":"+request.raw().getServerPort()+"/register/"+initationID;
        result.put("invitationlink", invitatoinLink);
        
        return result;
    }
    
    public String getProtocol(String protocolString) {
    	if ( protocolString.toLowerCase().contains("https") ) {
    		return "https";
    	} else {
    		return "http";
    	}
    }

    @Override
    public Permission getPermission() {
        return Permission.MANAGE_USERS;
    }

}
