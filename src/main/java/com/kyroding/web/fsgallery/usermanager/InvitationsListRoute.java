package com.kyroding.web.fsgallery.usermanager;

import java.util.HashMap;

import spark.Request;
import spark.Response;

import com.kyroding.web.fsgallery.I18nManager;
import com.kyroding.web.fsgallery.Permission;
import com.kyroding.web.fsgallery.RequestFacade;
import com.kyroding.web.fsgallery.User;
import com.kyroding.web.fsgallery.UserManager;
import com.kyroding.web.fsgallery.ViewRenderer;
import com.kyroding.web.fsgallery.route.PermissionRoute;

public class InvitationsListRoute extends PermissionRoute {

    public InvitationsListRoute() {
        super(RequestFacade.getInstance().getContextPath() + "/usermanager/getinvitations");
    }

    @Override
    public Object execute(Request request, Response response) {

        response.header("Content-Type", "text/html;charset=utf-8");
        HashMap<String, Object> contextMap = new HashMap<String, Object>();
        contextMap.put("userManager", UserManager.getInstance());
        String baseUrl = getProtocol(request.raw().getProtocol())+"://"+request.raw().getServerName()+":"+request.raw().getServerPort()+"/register/";
        contextMap.put("invitationBaseUrl", baseUrl);
        User user = (User)request.session().attribute("user");
        contextMap.put("user", user);
        contextMap.put("i18n", I18nManager.getInstance().getUserBundle(user));
		
        return ViewRenderer.renderVelocity("vm/invitationslist.vm", contextMap);
    }

    public String getProtocol(String protocolString) {
    	if ( protocolString.toLowerCase().contains("https") ) {
    		return "https";
    	} else {
    		return "http";
    	}
    }
    
    @Override
    public Permission getPermission() {
        return Permission.MANAGE_USERS;
    }
}
