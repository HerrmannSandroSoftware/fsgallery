package com.kyroding.web.fsgallery;

import java.util.Locale;
import java.util.ResourceBundle;



public class I18nManager {
	
	private static I18nManager instance;
	
	private I18nManager() {
		 
	}
	
	public ResourceBundle getUserBundle(User user) {
		Locale locale = user.getLocale();
		ResourceBundle bundle = Utf8ResourceBundle.getBundle( "i18n.fsgallery", locale);
		return bundle;
	}
	
	
	public static final I18nManager getInstance() {
		
		if(instance == null) {
			instance = new I18nManager();
		}		
		return instance;
	}

	public Object getDefaultUserBundle() {
		ResourceBundle bundle = Utf8ResourceBundle.getBundle( "i18n.fsgallery", RequestFacade.GALLERY_DEFAULT_LANGUAGE);
		return bundle;
	}
	
}
