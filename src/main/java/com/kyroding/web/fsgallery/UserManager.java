package com.kyroding.web.fsgallery;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;
import java.util.UUID;

import org.apache.commons.lang.StringUtils;

public class UserManager {

    private static UserManager instance;
    private List<User> users;
    private Map<Permission, Set<User>> userPermissions;
    private File userPermissionsFile = new File(RequestFacade.getGalleryWorkingDirectory().getAbsolutePath()
            + File.separator + "permissions.config");
    private File userInvitationsFile = new File(RequestFacade.getGalleryWorkingDirectory().getAbsolutePath()
            + File.separator + "invitations.config");
    private File userConfigFile = new File(RequestFacade.getGalleryWorkingDirectory().getAbsolutePath() + File.separator
            + "users.config");
    private File userLanguageFile = new File(RequestFacade.getGalleryWorkingDirectory().getAbsolutePath() + File.separator
            + "userlanguage.config");

    private UserManager() {
        initApplicationUsers();
        try {
            initUserPermissions();
        } catch (Exception e) {
            System.err.println("Error during init userpermissions: " + e.getMessage());
        }
        System.out.println("Usercache built");
    }

  
        
    private void initUserPermissions() throws FileNotFoundException, IOException {
        userPermissions = new HashMap<Permission, Set<User>>();
        if (userPermissionsFile.exists() || (!userPermissionsFile.exists() && userPermissionsFile.createNewFile())) {
            Properties permissionProperties = GalleryUtils.getPropertiesFromFile(userPermissionsFile);

            if (permissionProperties.size() != Permission.values().length) {
                writeBasicPermissions(permissionProperties);
                GalleryUtils.storePropertiesToFile(permissionProperties, userPermissionsFile);
            }

            for (Entry<Object, Object> permissionEntry : permissionProperties.entrySet()) {
                Permission currentPermission = Permission.valueOf(permissionEntry.getKey().toString());
                if (currentPermission != null) {
                    Set<User> userSet = userPermissions.get(currentPermission);
                    if (userSet == null) {
                        userSet = new HashSet<User>();
                        userPermissions.put(currentPermission, userSet);
                    }

                    String[] userStringArray = permissionEntry.getValue().toString().split(",");
                    for (String userName : userStringArray) {
                        User user = getUser(userName);
                        if (user != null) {
                            userSet.add(user);
                        }
                    }
                }
            }
        }
    }

    public User getUser(String userName) {
        for (User currentUser : users) {
            if (currentUser.getUsername().equals(userName)) {
                return currentUser;
            }
        }
        return null;
    }

    public boolean hasUserPermission(String permission, User user) {
        return hasUserPermission(Permission.valueOf(permission), user);
    }

    public boolean hasUserPermission(Permission permission, User user) {

        if (permission != null) {
            Set<User> userList = userPermissions.get(permission);
            if (userList.contains(user)) {
                return true;
            }
        }
        return false;
    }

    

    private void writeBasicPermissions(Properties permissionProperties) {
        for (Permission permission : Permission.values()) {
            if (permissionProperties.getProperty(permission.toString()) == null) {
                permissionProperties.setProperty(permission.toString(), "");
            }
        }
    }

    private void initApplicationUsers() {
        users = new ArrayList<User>();
        
        try {
            if ((userConfigFile.exists() || (!userConfigFile.exists() && userConfigFile.createNewFile())) &&
            		(userLanguageFile.exists() || (!userLanguageFile.exists() && userLanguageFile.createNewFile()))) {
                
            	Properties userProperties = GalleryUtils.getPropertiesFromFile(userConfigFile);
            	Properties userLanguageProperties = GalleryUtils.getPropertiesFromFile(userLanguageFile);
            	
            	for (Entry<Object, Object> userEntry : userProperties.entrySet()) {
                    
            		String username = userEntry.getKey().toString();
            		Locale userLocale = userLanguageProperties.getProperty(username) != null ? new Locale(userLanguageProperties.getProperty(username)) : RequestFacade.GALLERY_DEFAULT_LANGUAGE;
            		
					User currentUser = new User(username, userEntry.getValue().toString(), userLocale);
                    this.users.add(currentUser);
                }
                System.out.println(users.size() + " users found");
            }
        } catch (IOException e) {
            System.err.println("Unable to create " + userConfigFile.getName());
        }
    }

    public void setPermission(User user, Permission permission) {
        if (user != null && permission != null) {
            userPermissions.get(permission).add(user);
        }
        updateUserPermissionsToFilesystem();
    }

    public void removePermission(User user, Permission permission) {
        if (user != null && permission != null) {
            userPermissions.get(permission).remove(user);
        }
        updateUserPermissionsToFilesystem();
    }

    private void updateUserPermissionsToFilesystem() {
        try {
            Properties permissionProperties = new Properties();
            for (Entry<Permission, Set<User>> permissionEntry : userPermissions.entrySet()) {
                permissionProperties.setProperty(permissionEntry.getKey().toString(),
                        StringUtils.join(permissionEntry.getValue(), ","));
            }
            FileWriter writer = new FileWriter(userPermissionsFile);
            permissionProperties.store(writer, "");
            writer.close();

        } catch (IOException e) {
            System.err.println("Error during Properties update: " + e.getMessage());
        }

    }

    public List<User> getUsers() {
        return users;
    }

    public Map<Permission, Set<User>> getPermissionMap() {
        return this.userPermissions;
    }

    public static final UserManager getInstance() {

        if (instance == null) {
            instance = new UserManager();
        }
        return instance;
    }

	public String addInvitation(Set<Permission> permissions) {
		 try {
			 if (userInvitationsFile.exists() || (!userInvitationsFile.exists() && userInvitationsFile.createNewFile())) {
				 Properties propertiesFromFile = GalleryUtils.getPropertiesFromFile(userInvitationsFile);
				 String uid = UUID.randomUUID().toString();
				 propertiesFromFile.setProperty(uid, StringUtils.join(permissions, ","));
				 GalleryUtils.storePropertiesToFile(propertiesFromFile, userInvitationsFile);
				 return uid;
			 }
		} catch (IOException e) {
			System.err.println("Error handling the invitation file: "+e.getMessage());
		}
		return null;
	}
	
	public Map<String, Set<Permission>> getInvitations() {
		 try {
			 if (userInvitationsFile.exists() || (!userInvitationsFile.exists() && userInvitationsFile.createNewFile())) {
				 Properties propertiesFromFile = GalleryUtils.getPropertiesFromFile(userInvitationsFile);
				 Map<String, Set<Permission>> invitationsMap = new HashMap<String, Set<Permission>>();
				 
				 for (Entry<Object, Object> invitationEntry : propertiesFromFile.entrySet()) {
					 String[] permissionsSplit = invitationEntry.getValue().toString().split(",");
					 Set<Permission> permissionsSet = new HashSet<Permission>();
					 for (String permissionsString : permissionsSplit) {
						permissionsSet.add(Permission.valueOf(permissionsString.toUpperCase()));
					 }
					 
					 invitationsMap.put(invitationEntry.getKey().toString(), permissionsSet);
				 }
				 return invitationsMap;
			 }
		} catch (IOException e) {
			System.err.println("Error handling the invitation file: "+e.getMessage());
		}
		return null;
	}

	public void deleteInvitation(String invitationid) {
		 try {
			if (userInvitationsFile.exists() || (!userInvitationsFile.exists() && userInvitationsFile.createNewFile())) {
				 Properties propertiesFromFile = GalleryUtils.getPropertiesFromFile(userInvitationsFile);
				 propertiesFromFile.remove(invitationid);
				 GalleryUtils.storePropertiesToFile(propertiesFromFile, userInvitationsFile);
			 }
		} catch (IOException e) {
			System.err.println("Error during delete invitation: "+e.getMessage());
		}
		
	}

	public boolean addNewUser(User user, Set<Permission> permissions) {
		try {
			if (user != null && userConfigFile.exists() || (!userConfigFile.exists() && userConfigFile.createNewFile())) {
				 Properties propertiesFromFile = GalleryUtils.getPropertiesFromFile(userConfigFile);
				 propertiesFromFile.setProperty(user.getUsername(), user.getPassword());
				 GalleryUtils.storePropertiesToFile(propertiesFromFile, userConfigFile);
				 users.add(user);
				 
				 
				 for(Permission permission : permissions) {
					 userPermissions.get(permission).add(user);
				 }
				 updateUserPermissionsToFilesystem();
				 
				 return true;
			 }
		} catch (IOException e) {
			System.err.println("Error during add user: "+e.getMessage());
		}
		return false;
	}



	public boolean setUserLanguage(User user, Locale locale) {
		
		if(user != null) {
			try {
				Properties propertiesFromFile = GalleryUtils.getPropertiesFromFile(userLanguageFile);
				propertiesFromFile.setProperty(user.getUsername(), locale.getLanguage());
				GalleryUtils.storePropertiesToFile(propertiesFromFile, userLanguageFile);
				initApplicationUsers();
				return true;
			} catch (IOException e) {
				System.err.println("Error setting locale "+locale+" for user "+user.getUsername());
			}
		}
		return false;
	}

}
