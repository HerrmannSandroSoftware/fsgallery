package com.kyroding.web.fsgallery.filter;

import java.util.Arrays;
import java.util.HashSet;

import spark.Filter;
import spark.Request;
import spark.Response;

import com.kyroding.web.fsgallery.Permission;
import com.kyroding.web.fsgallery.RequestFacade;
import com.kyroding.web.fsgallery.UserManager;

public class AuthorizationFilter extends Filter {

    public AuthorizationFilter() {
        super(RequestFacade.getInstance().getContextPath() + "/*");
    }

    @Override
    public void handle(Request request, Response response) {

        String requestMethod = request.url();
        if (requestMethod.endsWith("/login")) {
            return;
        }

        if (UserManager.getInstance().getUsers().size() == 0) {
            String invitationId = UserManager.getInstance().addInvitation(
                    new HashSet<Permission>(Arrays.asList(Permission.values())));
            response.redirect("/register/" + invitationId);
            return;
        }

        if (request.session().attribute("user") != null) {

        } else {
            response.redirect("/login");
        }
    }
}
