package com.kyroding.web.fsgallery.filter;

import com.kyroding.web.fsgallery.RequestFacade;

import spark.Filter;
import spark.Request;
import spark.Response;

public class CompatibilityFilter extends Filter {

	public CompatibilityFilter() {
		super("/Pictures/handle");
	}

	@Override
	public void handle(Request request, Response response) {
		response.redirect(RequestFacade.getInstance().getContextPath()+"/view");
	}

}
