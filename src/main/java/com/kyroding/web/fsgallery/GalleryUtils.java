package com.kyroding.web.fsgallery;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Properties;

public class GalleryUtils {

	private GalleryUtils() {}
	
	public static Properties getPropertiesFromFile(File propertyFile) throws IOException {
		InputStreamReader inputStreamReader = new InputStreamReader(new FileInputStream(propertyFile), ViewRenderer.DEFAULT_ENCODING);
        Properties properties = new Properties();
        properties.load(inputStreamReader);
        inputStreamReader.close();
        return properties;
	}
	
	public static Properties getPropertiesFromFile(InputStream inStream) throws IOException {
		Properties properties = new Properties();
        properties.load(inStream);
        inStream.close();
        return properties;
	}
	
	
	public static void storePropertiesToFile(Properties properties, File file) {
        try {
            OutputStreamWriter outwriter = new OutputStreamWriter(new FileOutputStream(file), ViewRenderer.DEFAULT_ENCODING);
            properties.store(outwriter, "");
            outwriter.close();
        } catch (IOException e) {
            System.err.println("Error writing permissions file " + file.getName());
        }
    }
}
