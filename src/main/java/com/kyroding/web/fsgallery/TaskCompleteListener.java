package com.kyroding.web.fsgallery;

public interface TaskCompleteListener {
    void notifyOfTaskComplete(final Task task);
}