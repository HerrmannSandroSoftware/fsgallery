package com.kyroding.web.fsgallery;

import java.util.Locale;

public class User {

	private String username;
	private String password;
	private Locale locale;
	
	public User(String username, String password) {
		this(username, password, RequestFacade.GALLERY_DEFAULT_LANGUAGE);
	}
	
	public User(String username, String password, Locale locale) {
		this.username = username;
		this.password = password;
		this.locale = locale;
	}

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((username == null) ? 0 : username.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (username == null) {
			if (other.username != null)
				return false;
		} else if (!username.equals(other.username))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return username;
	}

	public Locale getLocale() {
		return locale;
	}

	public void setLocale(Locale locale) {
		this.locale = locale;
	}
}
