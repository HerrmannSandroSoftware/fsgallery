package com.kyroding.web.fsgallery;

public enum Permission {
    USE_GALLERY("album.use.gallery"), ADD_ALBUM("album.add"), COMMENT_IMAGE("album.imagea.comment"), MANAGE_USERS(
            "album.manage.users");

    private String permissonI18nKey;

    Permission(String permissonI18nKey) {
        this.permissonI18nKey = permissonI18nKey;
    }

    public String getPermissonI18nKey() {
        return permissonI18nKey;
    }
}