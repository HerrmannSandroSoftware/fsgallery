package com.kyroding.web.fsgallery;

import java.io.File;
import java.io.FilenameFilter;

public class PictureFileNameFilter implements FilenameFilter {

	public boolean accept(File file, String name) {
		return new File(file+File.separator+name).isFile() && (name.toLowerCase().endsWith(".jpeg") || name.toLowerCase().endsWith(".jpg") || name.toLowerCase().endsWith(".png") || name.toLowerCase().endsWith(".gif"));
	}

}
