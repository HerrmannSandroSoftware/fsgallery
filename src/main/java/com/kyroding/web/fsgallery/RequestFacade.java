package com.kyroding.web.fsgallery;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Locale;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Scanner;

import spark.Spark;

import com.kyroding.web.fsgallery.album.add.route.AddAlbumRoute;
import com.kyroding.web.fsgallery.album.add.route.AddAlbumStatusRoute;
import com.kyroding.web.fsgallery.album.add.route.GetRawAlbumsRoute;
import com.kyroding.web.fsgallery.album.delete.DeleteAlbumRoute;
import com.kyroding.web.fsgallery.album.get.route.AlbumTimelineRoute;
import com.kyroding.web.fsgallery.album.get.route.GetAlbumRoute;
import com.kyroding.web.fsgallery.album.imageactions.route.AddCommentRoute;
import com.kyroding.web.fsgallery.album.imageactions.route.SetCoverImageRoute;
import com.kyroding.web.fsgallery.filter.AuthorizationFilter;
import com.kyroding.web.fsgallery.filter.CompatibilityFilter;
import com.kyroding.web.fsgallery.filter.StartFilter;
import com.kyroding.web.fsgallery.route.LoginRoute;
import com.kyroding.web.fsgallery.route.LogoutRoute;
import com.kyroding.web.fsgallery.route.PictureRoute;
import com.kyroding.web.fsgallery.route.RegisterRoute;
import com.kyroding.web.fsgallery.route.ResourceRoute;
import com.kyroding.web.fsgallery.route.ViewRoute;
import com.kyroding.web.fsgallery.usermanager.DeleteInvitationRoute;
import com.kyroding.web.fsgallery.usermanager.GenerateInvitationRoute;
import com.kyroding.web.fsgallery.usermanager.GetUserdataRoute;
import com.kyroding.web.fsgallery.usermanager.InvitationsListRoute;
import com.kyroding.web.fsgallery.usermanager.RemoveUserPermissionRoute;
import com.kyroding.web.fsgallery.usermanager.SetUserLanguageRoute;
import com.kyroding.web.fsgallery.usermanager.SetUserPermissionRoute;

public class RequestFacade  {

	private static final String APPLICATION_VERSION = "application.version";
	private static final String APPLICATION_NAME = "application.name";
	private static final String APP_PROPERTY_GALLERY_PORT = "gallery.port";
	public static final String APP_PROPERTY_GALLERY_DIRECTORY = "gallery.directory";
	private static final String APP_PROPERTY_GALLERY_WORKING_DIRECTORY = "gallery.working.directory";
	private static final String APP_PROPERTY_GALLERY_DISPLAYNAME = "gallery.displayname";
	
	private Properties applicationProperties;
	private static RequestFacade instance;
		
	public static final Locale GALLERY_DEFAULT_LANGUAGE = Locale.GERMAN;
		
	public String getContextPath() {
		return "/" + getGalleryName();
	}
	
	public String getGalleryName() {
		return applicationProperties.getProperty(APPLICATION_NAME);
	}
	
	public String getGalleryVersion() {
		return applicationProperties.getProperty(APPLICATION_VERSION);
	}
	
	public RequestFacade(Properties applicationProperties) {
		this.applicationProperties = applicationProperties;
		RequestFacade.instance = this;
	}
	
	public static void main(String... args) {
		
		Properties appProps = new Properties();
		
		if(args.length == 3) {
			appProps.setProperty(APP_PROPERTY_GALLERY_PORT, args[0]);
			appProps.setProperty(APP_PROPERTY_GALLERY_DIRECTORY, args[1]);
			appProps.setProperty(APP_PROPERTY_GALLERY_DISPLAYNAME, args[2]);
		} else {
			
			Scanner in = new Scanner(System.in);
			System.out.print("On which port port should i run? ");
	        appProps.setProperty(APP_PROPERTY_GALLERY_PORT, in.nextLine());
	        
	        System.out.print("Where is your picture directory? (Do not use backslashes)");
	        appProps.setProperty(APP_PROPERTY_GALLERY_DIRECTORY, in.nextLine());
	        
	        System.out.print("What should be the name of the gallery? ");
	        appProps.setProperty(APP_PROPERTY_GALLERY_DISPLAYNAME, in.nextLine());
	        in.close();
		}
		
		new RequestFacade(appProps).init();
	}
	
	public void init() {
		initApplicationPropeties();
		initApplicationWorkfolder();
		AlbumManager.getInstance();
		
		String portProperty = applicationProperties.getProperty(APP_PROPERTY_GALLERY_PORT, "11000");
		Spark.setPort(Integer.parseInt(portProperty));
		Spark.before(new StartFilter());
		Spark.before(new CompatibilityFilter());
		Spark.before(new AuthorizationFilter());
		Spark.get(new LoginRoute());
		Spark.post(new LoginRoute());
		Spark.get(new RegisterRoute());
		Spark.post(new RegisterRoute());
		Spark.get(new LogoutRoute());
		Spark.get(new SetUserLanguageRoute());
		Spark.get(new ViewRoute(applicationProperties.getProperty(APP_PROPERTY_GALLERY_DISPLAYNAME)));
		Spark.get(new ResourceRoute());
		Spark.get(new PictureRoute());
		Spark.post(new GetRawAlbumsRoute());
		Spark.post(new AddAlbumRoute());
		Spark.post(new DeleteAlbumRoute());
		Spark.post(new AddAlbumStatusRoute());
		Spark.post(new AddCommentRoute());
		Spark.post(new SetCoverImageRoute());
		Spark.post(new AlbumTimelineRoute());
		Spark.post(new GetAlbumRoute());
		Spark.post(new GetUserdataRoute());
		Spark.post(new RemoveUserPermissionRoute()); 
		Spark.post(new SetUserPermissionRoute());
		Spark.post(new GenerateInvitationRoute());
		Spark.post(new DeleteInvitationRoute());
		Spark.get(new InvitationsListRoute());
	}
		
	private void initApplicationPropeties() {
		InputStream resourceAsStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("application.properties");
	    try {
			Properties properties = GalleryUtils.getPropertiesFromFile(resourceAsStream);
			for (Entry<Object,Object> entry : properties.entrySet()) {
				applicationProperties.setProperty(entry.getKey().toString(), entry.getValue().toString());
			}
		} catch (IOException e) {
			System.err.println("error reading applicaiton properties: "+e.getMessage());
		}
	}

	private void initApplicationWorkfolder() {
		String galleryDirectoryString = applicationProperties.getProperty(APP_PROPERTY_GALLERY_DIRECTORY);
		File galleryDirectory = new File(galleryDirectoryString);
		if(galleryDirectory.exists() && galleryDirectory.isDirectory()) {
			File galleryWorkingDirectory = new File(galleryDirectory.getAbsolutePath()+File.separator+".fsgallery");
			applicationProperties.setProperty(APP_PROPERTY_GALLERY_WORKING_DIRECTORY, galleryWorkingDirectory.getAbsolutePath());
			if(!galleryWorkingDirectory.exists()) {
				System.out.println("Initializing working directory");
				galleryWorkingDirectory.mkdirs();
			}
			UserManager.getInstance();
		}
		
	}

	public static File getRawImageDirectory() {
		return new File(instance.applicationProperties.getProperty(APP_PROPERTY_GALLERY_DIRECTORY));
	}
	
	public static File getGalleryWorkingDirectory() {
		return new File(instance.applicationProperties.getProperty(APP_PROPERTY_GALLERY_WORKING_DIRECTORY));
	}
	
	public static RequestFacade getInstance() {
		return instance;
	}
}
