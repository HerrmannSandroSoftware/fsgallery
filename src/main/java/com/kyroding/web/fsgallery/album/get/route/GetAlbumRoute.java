package com.kyroding.web.fsgallery.album.get.route;

import java.util.HashMap;
import java.util.Map;

import spark.Request;
import spark.Response;

import com.kyroding.web.fsgallery.AlbumManager;
import com.kyroding.web.fsgallery.Permission;
import com.kyroding.web.fsgallery.RequestFacade;
import com.kyroding.web.fsgallery.route.JsonRoute;

public class GetAlbumRoute extends JsonRoute {

    public GetAlbumRoute() {
        super(RequestFacade.getInstance().getContextPath() + "/album/get/*");
    }

    @Override
    public Map<String, Object> buildJsonData(Request request, Response response) {
        Map<String, Object> result = new HashMap<String, Object>();

        String[] splat = request.splat();
        if (splat.length == 1) {
            result.put("album", AlbumManager.getInstance().getAlbumById(splat[0]));
        }

        return result;
    }

    @Override
    public Permission getPermission() {
        return Permission.USE_GALLERY;
    }

}
