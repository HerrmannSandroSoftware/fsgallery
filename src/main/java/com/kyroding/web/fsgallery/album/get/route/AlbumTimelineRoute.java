package com.kyroding.web.fsgallery.album.get.route;

import java.util.HashMap;
import java.util.Map;

import spark.Request;
import spark.Response;

import com.kyroding.web.fsgallery.AlbumManager;
import com.kyroding.web.fsgallery.Permission;
import com.kyroding.web.fsgallery.RequestFacade;
import com.kyroding.web.fsgallery.route.JsonRoute;

public class AlbumTimelineRoute extends JsonRoute {

    public AlbumTimelineRoute() {
        super(RequestFacade.getInstance().getContextPath() + "/album/get/timeline");
    }

    @Override
    public Map<String, Object> buildJsonData(Request request, Response response) {
        Map<String, Object> result = new HashMap<String, Object>();

        result.put("timeline", AlbumManager.getInstance().getAlbumsMap());
        return result;
    }

    @Override
    public Permission getPermission() {
        return Permission.USE_GALLERY;
    }

}
