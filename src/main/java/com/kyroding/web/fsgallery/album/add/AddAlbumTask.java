package com.kyroding.web.fsgallery.album.add;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.UUID;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

import com.kyroding.web.fsgallery.AlbumManager;
import com.kyroding.web.fsgallery.PictureFileNameFilter;
import com.kyroding.web.fsgallery.RequestFacade;
import com.kyroding.web.fsgallery.Task;

public class AddAlbumTask extends Task {

	private File sourceFolder;
	private Date albumDate;
	private String albumName;
	private String username;
	
	public AddAlbumTask(String username, File sourceFolder, Date albumDate, String albumName) {
		this.sourceFolder = sourceFolder;
		this.albumDate = albumDate;
		this.albumName = albumName;
		this.username = username;
	}
	
	@Override
	public void doRun() {
		try {
			migratePictures();
		} catch (Exception e) {
			status = "Error!";
		}
		AlbumManager.getInstance().rebuildCache();
		progress = 100;
	}
	
	private void migratePictures() throws Exception
	{
		Integer picturesLongestSide = 800;
		Integer thumbsLongestSide = 200;
		File galleryWorkingDirectoryFolder = RequestFacade.getGalleryWorkingDirectory();
		
		File destinationAlbumFolder = new File(galleryWorkingDirectoryFolder.getAbsolutePath()+"/"+UUID.randomUUID().toString());
		destinationAlbumFolder.mkdirs();
		File[] listFiles = sourceFolder.listFiles(new PictureFileNameFilter());
		List<File> imageList = Arrays.asList(listFiles);
		Properties albumProperties = new Properties();
		albumProperties.setProperty(AlbumManager.KEY_ALBUM_NAME, this.albumName);
		albumProperties.setProperty(AlbumManager.KEY_ALBUM_TIMESTAMP, this.albumDate.getTime()+"");
		albumProperties.setProperty(AlbumManager.KEY_ALBUM_COVERIMAGE, imageList.size() > 0 ? imageList.get(0).getName() : "");
		albumProperties.store(new FileWriter(destinationAlbumFolder+File.separator+"album.properties"), "Source Directory was "+sourceFolder.getAbsolutePath());
		processPictures(imageList, destinationAlbumFolder, picturesLongestSide, thumbsLongestSide);
	}
	
	// converting the pictures and write them to the filesystem
	private boolean processPictures(List<File> sourceFiles, File destinationAlbumFolder, int longestSide, int longestSideThumb) 
	{
		destinationAlbumFolder.mkdirs();
		File thumbsFolder = new File(destinationAlbumFolder+"/thumbs");
		thumbsFolder.mkdirs();
		
		try
		{
			for (int i = 0; i < sourceFiles.size(); i++) 
			{
				File file = sourceFiles.get(i);
				
				BufferedImage bufferedImg = ImageIO.read(file);
				int width = 0;
				int heigth = 0;
				int widthThumb = 0;
				int heigthThumb = 0;
				if(bufferedImg.getWidth() > bufferedImg.getHeight())
				{
					width = longestSide;   
					widthThumb = longestSideThumb;
					heigth = (longestSide * bufferedImg.getHeight()) / bufferedImg.getWidth();
					heigthThumb = (longestSideThumb * bufferedImg.getHeight()) / bufferedImg.getWidth();
					
				}
				else
				{
					heigth = longestSide;
					heigthThumb = longestSideThumb;
					width = (longestSide * bufferedImg.getWidth()) / bufferedImg.getHeight();
					widthThumb = (longestSideThumb * bufferedImg.getWidth()) / bufferedImg.getHeight();
				} 
				
				BufferedImage outImg = scalePicture(bufferedImg, width,heigth);
				writeToFileSystem(outImg, new File(destinationAlbumFolder+"/"+file.getName()));
				
				BufferedImage outImgThumb = scalePicture(outImg, widthThumb,heigthThumb);
				writeToFileSystem(outImgThumb, new File(thumbsFolder+"/"+file.getName()));
				progress = ((i+1) * 100 / sourceFiles.size()) - 1;
			}
			return true;
		} 
		catch (Throwable e) 
		{
			e.printStackTrace();
			return false;
		}
		
		
	}
	
	private BufferedImage scalePicture(BufferedImage bufferedImg , int width, int height)
	{
		Image img = new ImageIcon(bufferedImg).getImage();
		Image scaledImage = img.getScaledInstance(width, height, Image.SCALE_SMOOTH);
		BufferedImage outImg = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
		Graphics g = outImg.getGraphics();
		g.drawImage(scaledImage, 0, 0, null);
		g.dispose();
		return outImg;
	}
	
	private void writeToFileSystem(BufferedImage outImg, File file) throws Exception
	{
		FileOutputStream baos = new FileOutputStream(file);
		ImageIO.write(outImg, "jpg", baos);
		baos.close();
	}
	
	@Override
	public String getTaskId() {
		return username+"-"+AddAlbumTask.class.getSimpleName();
	}
	
}
