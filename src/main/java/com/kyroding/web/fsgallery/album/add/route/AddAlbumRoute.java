package com.kyroding.web.fsgallery.album.add.route;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.TreeMap;

import spark.Request;
import spark.Response;

import com.kyroding.web.fsgallery.Permission;
import com.kyroding.web.fsgallery.RequestFacade;
import com.kyroding.web.fsgallery.TaskManager;
import com.kyroding.web.fsgallery.ViewRenderer;
import com.kyroding.web.fsgallery.album.add.AddAlbumTask;
import com.kyroding.web.fsgallery.route.JsonRoute;

public class AddAlbumRoute extends JsonRoute {

    public AddAlbumRoute() {
        super(RequestFacade.getInstance().getContextPath() + "/album/add/addalbum");
    }

    @Override
    public Map<String, Object> buildJsonData(Request request, Response response) {
        Map<String, Object> result = new TreeMap<String, Object>();
        String rawAlbumName = request.queryParams("rawalbumname");
        String newAlbumDate = request.queryParams("newalbumdate");
        String newAlbumName = request.queryParams("newalbumname");

        try {
            rawAlbumName = URLDecoder.decode(rawAlbumName, ViewRenderer.DEFAULT_ENCODING);
            newAlbumName = URLDecoder.decode(newAlbumName, ViewRenderer.DEFAULT_ENCODING);
        } catch (UnsupportedEncodingException e1) {
            System.err.println("Encoding problem during add album");
        }

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Date date;
        try {
            date = df.parse(newAlbumDate);
        } catch (ParseException e) {
            date = new Date();
        }

        AddAlbumTask task = new AddAlbumTask("dummyuser", new File(RequestFacade.getRawImageDirectory()
                + File.separator + rawAlbumName), date, newAlbumName);
        TaskManager.getInstance().addTask(task);

        result.put("task", task.toString());
        return result;
    }

    @Override
    public Permission getPermission() {
        return Permission.ADD_ALBUM;
    }

}
