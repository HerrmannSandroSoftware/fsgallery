package com.kyroding.web.fsgallery.album.add.route;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import spark.Request;
import spark.Response;

import com.kyroding.web.fsgallery.Permission;
import com.kyroding.web.fsgallery.PictureFileNameFilter;
import com.kyroding.web.fsgallery.RequestFacade;
import com.kyroding.web.fsgallery.route.JsonRoute;

public class GetRawAlbumsRoute extends JsonRoute {

    public GetRawAlbumsRoute() {
        super(RequestFacade.getInstance().getContextPath() + "/album/add/getrawalbums");
    }

    @Override
    public Map<String, Object> buildJsonData(Request request, Response response) {
        Map<String, Object> result = new TreeMap<String, Object>();
        File rawAlbumsFolder = RequestFacade.getRawImageDirectory();

        for (File rawAlbum : rawAlbumsFolder.listFiles()) {
            String[] rawPictures = rawAlbum.list(new PictureFileNameFilter());
            if (rawAlbum.isDirectory() && rawPictures.length > 0) {
                Map<String, Object> metaData = new HashMap<String, Object>();
                metaData.put("count", rawPictures.length);
                metaData.put("modified", rawAlbum.lastModified());
                result.put(rawAlbum.getName(), metaData);
            }
        }

        return result;
    }

    @Override
    public Permission getPermission() {
        return Permission.ADD_ALBUM;
    }

}
