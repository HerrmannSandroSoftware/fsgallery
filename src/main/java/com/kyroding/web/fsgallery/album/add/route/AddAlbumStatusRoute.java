package com.kyroding.web.fsgallery.album.add.route;

import java.util.HashMap;
import java.util.Map;

import spark.Request;
import spark.Response;

import com.kyroding.web.fsgallery.Permission;
import com.kyroding.web.fsgallery.RequestFacade;
import com.kyroding.web.fsgallery.Task;
import com.kyroding.web.fsgallery.TaskManager;
import com.kyroding.web.fsgallery.album.add.AddAlbumTask;
import com.kyroding.web.fsgallery.route.JsonRoute;

public class AddAlbumStatusRoute extends JsonRoute {

    public AddAlbumStatusRoute() {
        super(RequestFacade.getInstance().getContextPath() + "/album/add/status");
    }

    @Override
    public Map<String, Object> buildJsonData(Request request, Response response) {
        Map<String, Object> result = new HashMap<String, Object>();

        Task task = TaskManager.getInstance().getTaskById("dummyuser-" + AddAlbumTask.class.getSimpleName());
        Map<String, Object> taskData = new HashMap<String, Object>();
        if (task != null) {

            taskData.put("id", task.getTaskId());
            taskData.put("type", task.getClass().getSimpleName());
            taskData.put("status", task.getStatus());
            taskData.put("progress", task.getProgress());
        }
        result.put("task", taskData);
        return result;
    }

    @Override
    public Permission getPermission() {
        return Permission.ADD_ALBUM;
    }

}
