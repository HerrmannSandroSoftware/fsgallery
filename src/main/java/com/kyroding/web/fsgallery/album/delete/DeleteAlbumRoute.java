package com.kyroding.web.fsgallery.album.delete;

import java.util.Map;
import java.util.TreeMap;

import spark.Request;
import spark.Response;

import com.kyroding.web.fsgallery.AlbumManager;
import com.kyroding.web.fsgallery.Permission;
import com.kyroding.web.fsgallery.RequestFacade;
import com.kyroding.web.fsgallery.route.JsonRoute;

public class DeleteAlbumRoute extends JsonRoute {

    public DeleteAlbumRoute() {
        super(RequestFacade.getInstance().getContextPath() + "/album/delete/*");
    }

    @Override
    public Map<String, Object> buildJsonData(Request request, Response response) {
        Map<String, Object> result = new TreeMap<String, Object>();
        String[] splat = request.splat();
        
        if(splat.length == 1) {
        	if(AlbumManager.getInstance().deleteAlbum(splat[0])) {
    			result.put("status", "0");
           }
        }
        
        return result;
    }

    @Override
    public Permission getPermission() {
        return Permission.ADD_ALBUM;
    }

}
