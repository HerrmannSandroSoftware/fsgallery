package com.kyroding.web.fsgallery.album.imageactions.route;

import java.util.HashMap;
import java.util.Map;

import spark.Request;
import spark.Response;

import com.kyroding.web.fsgallery.AlbumManager;
import com.kyroding.web.fsgallery.Permission;
import com.kyroding.web.fsgallery.RequestFacade;
import com.kyroding.web.fsgallery.route.JsonRoute;

public class SetCoverImageRoute extends JsonRoute {

    public SetCoverImageRoute() {
        super(RequestFacade.getInstance().getContextPath() + "/album/imageactions/setcoverimage/*/*");
    }

    @Override
    public Map<String, Object> buildJsonData(Request request, Response response) {
        Map<String, Object> result = new HashMap<String, Object>();
        String[] splat = request.splat();
        if(splat.length == 2 ) {
        	String albumId = splat[0];
        	String imageName = splat[1];
        
        	if(AlbumManager.getInstance().setAlbumCoverImage(albumId, imageName)) {
        		result.put("status", "0");
        		return result;
        	} 
        }
        
        result.put("status", "0");
        return result;
    }

    @Override
    public Permission getPermission() {
        return Permission.COMMENT_IMAGE;
    }

}
