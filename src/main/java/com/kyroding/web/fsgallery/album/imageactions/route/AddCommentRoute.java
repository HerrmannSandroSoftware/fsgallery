package com.kyroding.web.fsgallery.album.imageactions.route;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import spark.Request;
import spark.Response;

import com.kyroding.web.fsgallery.AlbumManager;
import com.kyroding.web.fsgallery.Permission;
import com.kyroding.web.fsgallery.RequestFacade;
import com.kyroding.web.fsgallery.route.JsonRoute;

public class AddCommentRoute extends JsonRoute {

    public AddCommentRoute() {
        super(RequestFacade.getInstance().getContextPath() + "/album/imageactions/comment/*");
    }

    @Override
    public Map<String, Object> buildJsonData(Request request, Response response) {
        Map<String, Object> result = new HashMap<String, Object>();
        String comment = request.queryParams("comment");
        String imgName = request.queryParams("image");
        
        if(StringUtils.isBlank(comment)) {
        	comment = "";
        }
        
        String[] splat = request.splat();
        if(splat.length == 1 ) {
        	String albumId = splat[0];
        
        	if(AlbumManager.getInstance().addImageComment(albumId, imgName, comment)) {
        		result.put("status", "0");
        		return result;
        	} 
        }
        
        result.put("status", "0");
        return result;
    }

    @Override
    public Permission getPermission() {
        return Permission.COMMENT_IMAGE;
    }

}
