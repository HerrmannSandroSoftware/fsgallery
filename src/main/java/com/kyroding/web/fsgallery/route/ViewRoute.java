package com.kyroding.web.fsgallery.route;

import java.util.HashMap;

import spark.Request;
import spark.Response;

import com.kyroding.web.fsgallery.AlbumManager;
import com.kyroding.web.fsgallery.I18nManager;
import com.kyroding.web.fsgallery.Permission;
import com.kyroding.web.fsgallery.RequestFacade;
import com.kyroding.web.fsgallery.User;
import com.kyroding.web.fsgallery.UserManager;
import com.kyroding.web.fsgallery.ViewRenderer;

public class ViewRoute extends PermissionRoute {

    private String galleryDisplayName;

	public ViewRoute(String galleryDisplayName) {
        super(RequestFacade.getInstance().getContextPath() + "/view");
        this.galleryDisplayName = galleryDisplayName;
    }

    @Override
    public Object execute(Request request, Response response) {

    	response.header("Content-Type", "text/html;charset=utf-8");
        HashMap<String, Object> contextMap = new HashMap<String, Object>();
        contextMap.put("timelineStructure", AlbumManager.getInstance().getStructuredTimeline());
        contextMap.put("albumManager", AlbumManager.getInstance());
        contextMap.put("userManager", UserManager.getInstance());
        contextMap.put("galleryname", RequestFacade.getInstance().getGalleryName());
        contextMap.put("galleryversion", RequestFacade.getInstance().getGalleryVersion());
        contextMap.put("galleryDisplayName", galleryDisplayName);
        
        User user = (User)request.session().attribute("user");
        contextMap.put("user", user);
        contextMap.put("i18n", I18nManager.getInstance().getUserBundle(user));
		
        
        return ViewRenderer.renderVelocity("vm/view.vm", contextMap);
    }

    @Override
    public Permission getPermission() {
        return Permission.USE_GALLERY;
    }
}
