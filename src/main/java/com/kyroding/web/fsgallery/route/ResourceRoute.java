package com.kyroding.web.fsgallery.route;

import java.net.URLDecoder;

import spark.Request;
import spark.Response;

import com.kyroding.web.fsgallery.Permission;
import com.kyroding.web.fsgallery.RequestFacade;
import com.kyroding.web.fsgallery.ViewRenderer;

public class ResourceRoute extends PermissionRoute {

    public ResourceRoute() {
        super(RequestFacade.getInstance().getContextPath() + "/resources/*");
    }

    private void setMimeType(Response response, String path) {

        if (path.toLowerCase().endsWith(".css")) {
            response.type("text/css;charset=" + ViewRenderer.DEFAULT_ENCODING);
        } else if (path.toLowerCase().endsWith(".js")) {
            response.type("text/javascript;charset=" + ViewRenderer.DEFAULT_ENCODING);
        } else if (path.toLowerCase().endsWith(".png")) {
            response.type("image/png;charset=" + ViewRenderer.DEFAULT_ENCODING);
        } else if (path.toLowerCase().endsWith(".gif")) {
            response.type("image/gif;charset=" + ViewRenderer.DEFAULT_ENCODING);
        } else if (path.endsWith(".jpeg")) {
            response.type("image/jpeg;charset=" + ViewRenderer.DEFAULT_ENCODING);
        } else if (path.endsWith(".jpg")) {
            response.type("image/jpg;charset=" + ViewRenderer.DEFAULT_ENCODING);
        } else {
            response.type("text/plain;charset=" + ViewRenderer.DEFAULT_ENCODING);
        }
    }

    private void setCachingHeader(Response response) {
        final int CACHE_DURATION_IN_SECOND = 60 * 60 * 24 * 2; // 2 days
        final long CACHE_DURATION_IN_MS = CACHE_DURATION_IN_SECOND * 1000;
        long now = System.currentTimeMillis();

        response.header("Cache-Control", "max-age=" + CACHE_DURATION_IN_SECOND + ", public");
        response.header("Connection", "keep-alive");

        response.raw().setDateHeader("Date", now);
        response.raw().setDateHeader("Last-Modified", now);
        response.raw().setDateHeader("Expires", now + CACHE_DURATION_IN_MS);
    }

    @Override
    public Object execute(Request request, Response response) {
        String[] splat = request.splat();
        setCachingHeader(response);
        if (splat.length > 0) {

            try {
                String resourcePath = splat[0];

                byte[] byteArray = ViewRenderer.getResourceAsByteArray(URLDecoder.decode(resourcePath,
                        ViewRenderer.DEFAULT_ENCODING));
                setMimeType(response, resourcePath);
                response.raw().getOutputStream().write(byteArray, 0, byteArray.length);
                return "";
            } catch (Exception e) {
                e.printStackTrace();
                response.status(404);
            }
        } else {
            response.status(404);
        }
        return "";
    }

    @Override
    public Permission getPermission() {
        return Permission.USE_GALLERY;
    }

}
