package com.kyroding.web.fsgallery.route;

import spark.Request;
import spark.Response;
import spark.Route;

import com.kyroding.web.fsgallery.Permission;
import com.kyroding.web.fsgallery.User;
import com.kyroding.web.fsgallery.UserManager;

public abstract class PermissionRoute extends Route {

    protected PermissionRoute(String path) {
        super(path);
    }

    @Override
    public final Object handle(Request request, Response response) {

        User user = (User) request.session().attribute("user");
        if (user != null && UserManager.getInstance().hasUserPermission(getPermission(), user)) {
            return execute(request, response);
        }

        response.status(401);
        return "";
    }

    public abstract Object execute(Request request, Response response);

    public abstract Permission getPermission();
}
