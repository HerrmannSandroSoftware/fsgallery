package com.kyroding.web.fsgallery.route;

import java.io.File;
import java.io.FileInputStream;
import java.net.URLDecoder;

import org.apache.commons.io.IOUtils;

import spark.Request;
import spark.Response;

import com.kyroding.web.fsgallery.Permission;
import com.kyroding.web.fsgallery.RequestFacade;
import com.kyroding.web.fsgallery.ViewRenderer;

public class PictureRoute extends PermissionRoute {

    public PictureRoute() {
        super(RequestFacade.getInstance().getContextPath() + "/pictures/*/*");
    }

    private void setMimeType(Response response, String path) {

        if (path.toLowerCase().endsWith(".png")) {
            response.type("image/png;charset=" + ViewRenderer.DEFAULT_ENCODING);
        } else if (path.toLowerCase().endsWith(".gif")) {
            response.type("image/gif;charset=" + ViewRenderer.DEFAULT_ENCODING);
        } else if (path.toLowerCase().endsWith(".jpeg")) {
            response.type("image/jpeg;charset=" + ViewRenderer.DEFAULT_ENCODING);
        } else if (path.toLowerCase().endsWith(".jpg")) {
            response.type("image/jpg;charset=" + ViewRenderer.DEFAULT_ENCODING);
        }
    }

    private void setCachingHeader(Response response) {
        final int CACHE_DURATION_IN_SECOND = 60 * 60 * 24 * 2; // 2 days
        final long CACHE_DURATION_IN_MS = CACHE_DURATION_IN_SECOND * 1000;
        long now = System.currentTimeMillis();

        response.header("Cache-Control", "max-age=" + CACHE_DURATION_IN_SECOND + ", public");
        response.header("Connection", "keep-alive");

        response.raw().setDateHeader("Date", now);
        response.raw().setDateHeader("Last-Modified", now);
        response.raw().setDateHeader("Expires", now + CACHE_DURATION_IN_MS);
    }

    @Override
    public Object execute(Request request, Response response) {
        String[] splat = request.splat();
        setCachingHeader(response);
        if (splat.length > 0) {

            try {
                String resourcePath = RequestFacade.getGalleryWorkingDirectory().getAbsolutePath() + File.separator
                        + splat[0] + File.separator + splat[1];

                FileInputStream fileInputStream = new FileInputStream(URLDecoder.decode(resourcePath,
                        ViewRenderer.DEFAULT_ENCODING));
                byte[] byteArray = IOUtils.toByteArray(fileInputStream);
                fileInputStream.close();
                setMimeType(response, resourcePath);
                response.raw().getOutputStream().write(byteArray, 0, byteArray.length);
                return byteArray;
            } catch (Exception e) {
                e.printStackTrace();
                response.status(404);
            }
        } else {
            response.status(404);
        }
        return "";
    }

    @Override
    public Permission getPermission() {
        return Permission.USE_GALLERY;
    }

}
