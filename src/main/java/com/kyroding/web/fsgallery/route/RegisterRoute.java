package com.kyroding.web.fsgallery.route;

import java.util.HashMap;
import java.util.Set;

import org.apache.commons.codec.digest.DigestUtils;

import spark.Request;
import spark.Response;
import spark.Route;

import com.kyroding.web.fsgallery.I18nManager;
import com.kyroding.web.fsgallery.Permission;
import com.kyroding.web.fsgallery.RequestFacade;
import com.kyroding.web.fsgallery.User;
import com.kyroding.web.fsgallery.UserManager;
import com.kyroding.web.fsgallery.ViewRenderer;

public class RegisterRoute extends Route {

	private static final String KEY_ERROR = "error";

	public RegisterRoute() {
		super("/register/*");
	}

	@Override
	public Object handle(Request request, Response response) {
		
		HashMap<String, Object> contextMap = new HashMap<String, Object>();
		contextMap.put("request", request);
		response.header("Content-Type", "text/html;charset=utf-8");
		String requestMethod = request.requestMethod();
		String[] splat = request.splat();
		
		if(splat.length == 1 ) {
			String invitationId = splat[0];
			Set<Permission> permissions = UserManager.getInstance().getInvitations().get(invitationId);
			if(permissions == null) {
				contextMap.put(KEY_ERROR, "register.key.invalid");
			} else {
				
				if(requestMethod.equals("post")) {
					String username = request.queryParams("username");
					String password = request.queryParams("password");
					String repassword = request.queryParams("repassword");
					
					if( username.length() < 1 || !username.matches("\\w*")) {
						contextMap.put(KEY_ERROR, "register.username.invalid");
					}
					else if( UserManager.getInstance().getUser(username) != null) {
						contextMap.put(KEY_ERROR, "register.username.exists");
					}
					else if(password.length() < 6) {
						contextMap.put(KEY_ERROR, "register.password.toshort");
					}
					else if(!password.equals(repassword)) {
						contextMap.put(KEY_ERROR, "register.password.different");
					}
					
					if(contextMap.get(KEY_ERROR) == null) {
						User newUser = new User(username,  DigestUtils.md5Hex(password));
						UserManager.getInstance().addNewUser(newUser, permissions);
						UserManager.getInstance().deleteInvitation(invitationId);
						request.session().attribute("user", newUser);
						response.redirect(RequestFacade.getInstance().getContextPath()+"/view");
					}
				}
			}
		}
		
		contextMap.put("i18n", I18nManager.getInstance().getDefaultUserBundle());
		return ViewRenderer.renderVelocity("vm/register.vm", contextMap);
	}
	
}
