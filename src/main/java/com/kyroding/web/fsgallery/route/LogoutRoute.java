package com.kyroding.web.fsgallery.route;

import spark.Request;
import spark.Response;
import spark.Route;

public class LogoutRoute extends Route {

	public LogoutRoute() {
		super("/logout");
	}

	@Override
	public Object handle(Request request, Response response) {
		
		request.session().attribute("user", null);
		response.redirect("/login");
		return "";
	}

}
