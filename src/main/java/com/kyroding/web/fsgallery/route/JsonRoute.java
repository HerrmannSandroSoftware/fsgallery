package com.kyroding.web.fsgallery.route;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import spark.Request;
import spark.Response;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.kyroding.web.fsgallery.ViewRenderer;

public abstract class JsonRoute extends PermissionRoute {

    protected JsonRoute(String path) {
        super(path);
    }

    @Override
    public final Object execute(Request request, Response response) {
        response.type("application/json");
        response.raw().setCharacterEncoding(ViewRenderer.DEFAULT_ENCODING);
        response.header("Cache-Control", "no-cache, no-store, public");
        return renderJSON(buildJsonData(request, response));
    }

    public abstract Map<String, Object> buildJsonData(Request request, Response response);

    public String renderJSON(Map<String, Object> content) {
        Iterator<Entry<String, Object>> it = content.entrySet().iterator();

        Gson gson = new Gson();
        JsonObject jsonObject = new JsonObject();

        while (it.hasNext()) {
            Map.Entry<String, Object> pairs = it.next();
            JsonElement jsonTree = gson.toJsonTree(pairs.getValue());
            jsonObject.add(pairs.getKey(), jsonTree);
        }

        return jsonObject.toString();
    }
}
