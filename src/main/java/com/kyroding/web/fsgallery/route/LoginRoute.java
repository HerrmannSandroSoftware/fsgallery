package com.kyroding.web.fsgallery.route;

import java.util.HashMap;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.StringUtils;

import spark.Request;
import spark.Response;
import spark.Route;

import com.kyroding.web.fsgallery.I18nManager;
import com.kyroding.web.fsgallery.RequestFacade;
import com.kyroding.web.fsgallery.User;
import com.kyroding.web.fsgallery.UserManager;
import com.kyroding.web.fsgallery.ViewRenderer;

public class LoginRoute extends Route {

	public LoginRoute() {
		super("/login");
	}

	@Override
	public Object handle(Request request, Response response) {
		
		HashMap<String, Object> contextMap = new HashMap<String, Object>();
		String requestMethod = request.requestMethod();
		if(requestMethod.equals("post")) {
			
			String username = request.queryParams("username");
			String password = request.queryParams("password");
			String destination = request.queryParams("destination");
			
			if(!StringUtils.isBlank(username) && !StringUtils.isBlank(password) && isLoginValid(request, username, DigestUtils.md5Hex(password))) {
				response.redirect(destination != null ? destination : RequestFacade.getInstance().getContextPath()+"/view");
			} else {
				contextMap.put("error", "login.incorrect");
			}
		}
		
	    contextMap.put("i18n", I18nManager.getInstance().getDefaultUserBundle());
		
		response.header("Content-Type", "text/html;charset=utf-8");
		return ViewRenderer.renderVelocity("vm/login.vm", contextMap);
	}

	private boolean isLoginValid(Request request, String username, String password) {
		for(User currentUser : UserManager.getInstance().getUsers()) {
			if(currentUser.getUsername().equals(username) && currentUser.getPassword().equals(password)) {
				request.session().attribute("user", currentUser);
				return true;
			}
		}
		return false;
	}
	
}
