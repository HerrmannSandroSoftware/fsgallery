package com.kyroding.web.fsgallery;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.io.IOUtils;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class ViewRenderer {
    private static VelocityEngine ve;
    public static final String DEFAULT_ENCODING = "utf-8";

    private static void initVelocityEngine() {
        ve = new VelocityEngine();
        ve.setProperty(RuntimeConstants.RESOURCE_LOADER, "classpath");
        ve.setProperty("classpath.resource.loader.class", ClasspathResourceLoader.class.getName());
        ve.init();
    }

    public static byte[] getResourceAsByteArray(String path) throws IOException {

        InputStream resourceAsStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(path);
        byte[] byteArray = IOUtils.toByteArray(resourceAsStream);
        resourceAsStream.close();
        return byteArray;
    }

    public static String renderVelocity(String templatePath, Map<String, Object> contextMap) {
        if (ve == null) {
            initVelocityEngine();
        }

        Template t = ve.getTemplate(templatePath, DEFAULT_ENCODING);

        VelocityContext context = new VelocityContext(contextMap);
        
        StringWriter writer = new StringWriter();
        t.merge(context, writer);
        String string = writer.toString();
		try {
			writer.close();
		} catch (IOException e) {
			System.err.println("Unable to close writer");
		}
        return string;
    }

    public static String getJSONFromMap(Map<String, Object> content) throws IOException {
        Iterator<Entry<String, Object>> it = content.entrySet().iterator();

        Gson gson = new Gson();
        JsonObject jsonObject = new JsonObject();

        while (it.hasNext()) {
            Map.Entry<String, Object> pairs = it.next();
            JsonElement jsonTree = gson.toJsonTree(pairs.getValue());
            jsonObject.add(pairs.getKey(), jsonTree);
        }

        return jsonObject.toString();
    }

    private ViewRenderer() {
    }
}
