package com.kyroding.web.fsgallery;

import java.util.ArrayList;
import java.util.List;


public class TaskManager implements TaskCompleteListener {
	
	private static TaskManager instance;
	private List<Task> runningTasks;
	private List<Task> completedTasks;
	
	private TaskManager() {
		runningTasks = new ArrayList<Task>();
		completedTasks = new ArrayList<Task>();
	}
		
	
	
	public static final TaskManager getInstance() {
		
		if(instance == null) {
			instance = new TaskManager();
		}		
		return instance;
	}

	public boolean addTask(Task task) {
		if(runningTasks.contains(task)) {
			return false;
		}
		task.addListener(this);
		runningTasks.add(task);
		task.start();
		return true;
	}
	
	public void notifyOfTaskComplete(Task task) {
		runningTasks.remove(task);
		if(completedTasks.size() >= 50) {
			completedTasks.remove(completedTasks.size()-1);
		}
		completedTasks.add(0,task);
	}



	public Task getTaskById(String taskId) {
		
		//first try the running ones
		for (Task runningTask : runningTasks) {
			if(runningTask.getTaskId().equals(taskId)) {
				return runningTask;
			}
		}
		
		//now the completed
		for (Task completedTask : completedTasks) {
			if(completedTask.getTaskId().equals(taskId)) {
				return completedTask;
			}
		}
		
		return null;
	}
}
